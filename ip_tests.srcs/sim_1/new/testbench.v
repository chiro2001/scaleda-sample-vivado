`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/28/2023 08:55:10 PM
// Design Name: 
// Module Name: testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench();
    reg clk;
    reg rst_n;
    test_source u_test (
        .clk  (clk),
        .rst_n(rst_n)
    );
    always #1 clk <= ~clk;
    initial begin
        clk <= 0;
        rst_n <= 0;
        #50000 rst_n <= 1;
        #100000 $finish;
    end
endmodule
