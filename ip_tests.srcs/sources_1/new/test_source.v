`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/22/2023 04:39:07 PM
// Design Name: 
// Module Name: test_source
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module instance_module (
    input clk,
    input rst_n,
    output [31:0] c
);
    reg [31:0] cnt;
    reg we;
    reg [31:0] addr;
    reg [31:0] din;
    wire [31:0] dout;

    assign c = dout;

//    single_mem u_single_mem (
//        .clk (clk),
//        .en  (1),
//        .we  (we),
//        .addr(addr),
//        .din (din),
//        .dout(dout)
//    );

    always @(posedge clk or negedge rst_n) begin
        if (!rst_n) begin
            cnt <= 0;
            we  <= 0;
        end else begin
            addr <= cnt;
            din  <= cnt ^ 32'h55;
            if (cnt != 31) cnt <= cnt + 1;
            else cnt <= 0;
        end
    end

//    my_ip u_my_ip ();
//
//    clocking_wizard_wrapper u_clocking_wizard_wrapper ();

//    bram_inner u_bram_inner (
    test_bram u_bram_inner (
//        .clk (clk),
//        .en  (1),
//        .we  (0),
//        .addr(0),
//        .din (0),
//        .dout()
        .clk (clk),
        .en  (1),
        .we  (we),
        .addr(addr),
        .din (din),
        .dout(dout)
    );
endmodule


module test_source (
    input clk,
    input rst_n,
    output [31:0] c
);
    wire clk_out;
    wire locked;
    wire rst_n_locked;
    assign rst_n_locked = rst_n && locked;
    clk_wiz_1 u_clk_wiz_1 (
        .clk_out1(clk_out),
        .reset(!rst_n),
        .locked(locked),
        .clk_in1(clk)
    );
    instance_module u_inst (
        .clk(clk),
        .rst_n(rst_n_locked),
        .c(c)
    );

endmodule
